# python3

class ActionStore():
	
	_tickInterval = 0.1
	
	_runSensors = False
	_runDecisions = False
	_runMoves = False
	
	_keepRunning = False
	
	_SensorReading = False
	_CurrentMove = False
	
	@property
	def tickInterval(self):
		return type(self)._tickInterval
		
	@tickInterval.setter
	def tickInterval(self, val):
		type(self)._tickInterval = val
		
		
	@property
	def runSensors(self):
		return type(self)._runSensors
		
	@runSensors.setter
	def runSensors(self, val):
		type(self)._runSensors = val
		
	
	@property
	def runDecisions(self):
		return type(self)._runDecisions
		
	@runDecisions.setter
	def runDecisions(self, val):
		type(self)._runDecisions = val
		
		
	@property
	def runMoves(self):
		return type(self)._runMoves
		
	@runMoves.setter
	def runMoves(self, val):
		type(self)._runMoves = val
		
	@property
	def keepRunning(self):
		return type(self)._keepRunning
		
	@keepRunning.setter
	def keepRunning(self, val):
		type(self)._keepRunning = val


	@property
	def SensorReading(self):
		return type(self)._SensorReading
		
	@SensorReading.setter
	def SensorReading(self, val):
		type(self)._SensorReading = val
		
		
	@property
	def CurrentMove(self):
		return type(self)._CurrentMove
		
	@CurrentMove.setter
	def CurrentMove(self, val):
		type(self)._CurrentMove = val

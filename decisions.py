# python3

from decision import Decision
from move import Move
import time
import threading
from action_store import ActionStore
from sensory_data import SensoryData

action_store = ActionStore()

last_decision = False

MINIMUM_RANGE = 10
MEDIUM_RANGE = 30
LONG_RANGE = 70

LOW_SPEED = 30
HIGH_SPEED = 70

def start_decisions():
	last_decision = False
	action_store.runDecisions = True

def run_decisions():
	left_distance = 100000
	middle_distance = 100000
	right_distance = 100000
	while action_store.runDecisions:
		time.sleep(action_store.tickInterval)
		sensor_data = action_store.SensorReading
		if sensor_data:
			
			# Piece of a rulebook for main behavior
			if (sensor_data.left < 4 and sensor_data.middle < 4 and sensor_data.right < 4):
				action_store.runSensors = False
			
			# filter out out-of-range
			# left
			if (sensor_data.left > LONG_RANGE or sensor_data.left < MINIMUM_RANGE):
				left_distance = 100000
			else:
				left_distance = sensor_data.left
			# middle
			if (sensor_data.middle > LONG_RANGE or sensor_data.middle < MINIMUM_RANGE):
				middle_distance = 100000
			else:
				middle_distance = sensor_data.middle
			# right
			if (sensor_data.right > LONG_RANGE or sensor_data.right < MINIMUM_RANGE):
				right_distance = 100000
			else:
				right_distance = sensor_data.right
			
			# check for closest range
			if (left_distance == 100000 and middle_distance == 100000 and right_distance == 100000):
				create_empty_decision()
			else:
				create_decision({"left": left_distance, "middle": middle_distance, "right": right_distance})
			
				
def create_decision(distances):
	# print (threading.currentThread().getName())
	smallest_distance = min(distances.values())
	direction = list(filter(lambda x: distances[x] == smallest_distance, distances))[0]
	distance = distances[direction]
	move_action = 'forward_march'
	move_speed = HIGH_SPEED
	if (direction == 'left'):
		move_action = 'spin_left'
	elif (direction == 'right'):
		move_action = 'spin_right'
		
	if (distance < MEDIUM_RANGE and move_action == 'forward_march'):
		move_speed = LOW_SPEED
	cm = action_store.CurrentMove
	if (cm.action != move_action or cm.speed != move_speed):
		action_store.keepRunning = False
		store_data(Move(move_action, move_speed))
				
def create_empty_decision():
	action_store.keepRunning = False
	store_data(Move(False, False))
	
def store_data(move):
	action_store.CurrentMove = move
	

	

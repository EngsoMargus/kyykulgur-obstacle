# python3

import sys
import time
import RPi.GPIO as GPIO
import threading
import sensors
import decisions
import moves

from action_store import ActionStore
action_store = ActionStore()

sensorsThread = False
decisionsThread = False
movementsThread = False

GPIO.setmode(GPIO.BOARD)

motor_pin_list = [16, 12, 22, 15, 18, 13]
GPIO.setup(motor_pin_list, GPIO.OUT)

def main():
	print("Sequence initaited!")
	action_store.tickInterval = 0.069
	
	sensorsThread = runThread("sensorsThread")
	sensorsThread.start()
	
	decisionsThread = runThread("decisionsThread")
	decisionsThread.start()
	
	movesThread = runThread("movesThread")
	movesThread.start()
	
	time.sleep(1)

	close_threads([movesThread, decisionsThread, sensorsThread])
	
def close_threads(threads):
	for thread in threads:
		if thread.name == 'movesThread':
			action_store.runMoves = False
		if thread.name == 'decisionsThread':
			action_store.runDecisions = False
		if thread.name == 'sensorsThread':
			action_store.runSensors = False
		thread.join()
	GPIO.cleanup()
	
class runThread(threading.Thread):
	def __init__(self, name):
		threading.Thread.__init__(self)
		self.name = name
	def run(self):
		if self.name == "movesThread":
			moves.start_moves()
			moves.run_moves()
		if self.name == "sensorsThread":
			sensors.start_sensors()
			sensors.run_sensors()
		if self.name == "decisionsThread":
			decisions.start_decisions()
			decisions.run_decisions()
	
def not_main():
	print("not main")

	
if __name__=='__main__':
	main()
else:
	not_main()

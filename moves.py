# python3

import sys
import time
import RPi.GPIO as GPIO
import threading
from action_store import ActionStore
from move import Move

action_store = ActionStore()

def start_moves():
	print ("Start Moves!")
	action_store.runMoves = True
	
def get_directive(current_move):
	current_directive = False
	if (current_move.action == "forward_march"):
		current_directive = Directive([12, 15, 16, 22], current_move.speed)
	elif (current_move.action == "spin_left"):
		current_directive = Directive([12, 13, 16, 22], current_move.speed)
	elif (current_move.action == "spin_right"):
		current_directive = Directive([12, 15, 16, 18], current_move.speed)
	return current_directive
	
def run_moves():
	while action_store.runSensors:		
		time.sleep(action_store.tickInterval)
		if (action_store.CurrentMove):
			current_move = action_store.CurrentMove
			current_directive = get_directive(current_move)
			if (current_directive):
				start_directive(current_directive)
			
def start_directive(current_directive):
	action_store.keepRunning = True
	# print (current_directive.pins, " : ", current_directive.speed)
	p1 = GPIO.PWM(12, 100)
	p2 = GPIO.PWM(16, 100)
	p1.start(current_directive.speed)
	p2.start(current_directive.speed)
	for pin in current_directive.pins:
		if pin:
			GPIO.output(pin,GPIO.HIGH)
	while (action_store.keepRunning):
		time.sleep(action_store.tickInterval)
	end_directive(current_directive)
			
def end_directive(ld):
	if (ld):
		for pin in ld.pins:
			if pin:
				GPIO.output(pin,GPIO.LOW)
			
class Directive:
	def __init__(self, pins, speed):
		self.pins = pins
		self.speed = speed
